.. sorzun documentation master file, created by
   sphinx-quickstart on Thu Oct 24 20:40:19 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Sorzun
======

.. meta::
   :description: Python modules for BIP32 and BIP32 HD deterministic wallets
   :keywords: cyprocurrency, BIP32, BIP39, HD Wallets


Sorzun is a simple python3 software library and set of tools for working with
Bitcoin and similar cryptocurrency system addresses and keys. Sorzun supports
BIP32 HD key tree functions, BIP39 Mnemonic functions, secp256k1 ECDSA math,
and various standardized address format conversions. Sorzon consists of python
modules which provide the base58 codec, BIP32 HD wallet generation, BIP39
Mnemonic generation, and cashaddr conversion capabilities which are exposed to
end users via a handful of command-line utilities and a python package.


Contents
========

.. toctree::
   :maxdepth: 2

   api/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
